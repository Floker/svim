#include "uservali.h"
#include <iostream>
#include <conio.h> // para el getch
#include <fstream>

#define ENTER 13
#define BACKSPACE 8
#define INTENTOSMAX 2



Uservali::Uservali()
{
    user="";
    pass="";
    readlist();
}

bool Uservali::Run()
{
    char caracter;
    int contador=0;
    do {
       user="";
       pass="";
       system("cls");
       cout << "Para ingresar al programa SVIM es necesario identificarse" << endl;
       cout << "----------------" << endl;
       cout << "Usuario: ";

       getline(cin, user);

       cout << "Password: ";

       caracter = getch();
       while (caracter != ENTER) {
             readPass(caracter);
             caracter = getch();
       }


       if(!(verificUserPass())){
             cout<<endl<< "\El usuario y/o password son incorrectos" << endl;
             contador++;
             cin.get();
       }

    } while (verificUserPass() == false && contador <= INTENTOSMAX);

    if (verificUserPass() == false && contador>INTENTOSMAX) {
       cout << endl <<"Usted no pudo ingresar al sistema." << endl;
    }
    system("cls");      //BORRO TODO DE LA PANTALLA



    if(user=="admin" && verificUserPass()){
        bool seguir=true;
        unsigned opcion;
        system("cls");
        cout<< "Bienvenido administrador"<<endl;
        while (seguir){
            cout<<endl<<endl<< "Ingrese el numero correspondiente a la tarea que desea realizar:"<<endl
                << "1. Crear usuario."<<endl
                << "2. Eliminar usuario."<<endl
                << "3. Modificar clave de usuario."<<endl
                << "4. Listar Usuarios."<<endl
                << "0. Lanzar SVIM."<<endl;
            cin>>opcion;

            if (opcion <= 4)
            {
                if (opcion == 1){
                    string usuario;
                    string clave;
                    cout<<"Ingrese nombre del usuario a crear y posteriormente su clave\n"
                          "Recuerde que la misma debe tener al menos:\n8 caracteres, "
                          "1 minus, 1 mayus, y 1 numero "<<endl;
                    cin>>usuario;
                    cin>>clave;
                    if(newUser(usuario, clave))
                    {
                     cout<<"El usuario ha sido creado con exito";
                    }
                    else
                    {
                     cout<<"El usuario ya existe o la clave es erronea";
                    }
                    cin.get();
                    seguir = true;

                 }else
                if (opcion == 2){
                    string nombre;
                    cout<<"Ingrese el nombre del usuario que desea eliminar"<<endl;
                    cout<<"Usuarios:"<<endl;
                    cout<<listarusuarios()<<endl;
                    cout<<"Usuario a eliminar:"<<endl;
                    cin>>nombre;

                    map<string,string>::iterator it;
                    it = m.find(nombre);
                    m.erase(it);

                    it = m.find(nombre);
                    if(it == m.end())
                    {cout<<"El usuario fue eliminado con exito";
                     savelist();
                    } else cout<<"El usuario no ha podido ser eliminado";
                 }else
                if (opcion == 3){
                    string usuario;
                    string clave;
                    cout<<"Ingrese nombre del usuario a modificar y posteriormente su nueva clave\n"
                          "Recuerde que la misma debe tener al menos:\n8 caracteres, "
                          "1 minus, 1 mayus, y 1 numero "<<endl;
                    cout<<"Usuarios:"<<endl;
                    cout<<listarusuarios()<<endl;
                    cout<<"Usuario a modificar:"<<endl;
                    cin>>usuario;
                    cout<<"Nueva clave:"<<endl;
                    cin>>clave;
                    if(modificarUser(usuario, clave))
                    {
                     cout<<"La clave fue modificada con exito";
                    }
                    else
                    {
                     cout<<"El usuario no existe o la clave es erronea";
                    }
                    cin.get();
                    seguir = true;
                 }else
                if (opcion == 4){
                     cout<<"Usuarios:"<<endl;
                     cout<<listarusuarios();
                 }else
                if (opcion == 0)
                    seguir = false;

            cout<<endl<<endl<<
                "------------------------------------"
                <<endl<<endl;
            }
            else
                cout<<endl<< "La opcion que ingreso no corresponde a una tarea." << endl << "Vuelva a ingresar una opcion." << endl << endl;
        }
    }

    bool var = verificUserPass();
    return var;
}


bool Uservali::newUser(string pUser, string pPass)
{
    bool retorno = false;
    map<string,string>::iterator it;
    it = m.find(pUser);
    if(it == m.end())
    {
        if(checkPass(pPass))
        {
           for(unsigned i=0; i<pPass.length(); i++)
           {
             pPass[i] += 5;
           }
        m[pUser]=pPass;
        savelist();
        retorno = true;
        }
    }
    return retorno;
}

bool Uservali::modificarUser(string pUser, string pPass)
{
    bool retorno = false;
    map<string,string>::iterator it;
    it = m.find(user);
    if(it != m.end())
    {
        if(checkPass(pPass))
        {
         for(unsigned i=0; i<pPass.length(); i++)
         {
          pPass[i] += 5;
         }
        m[pUser]=pPass;
        savelist();
        retorno = true;
        }
    }
    return retorno;
}

bool Uservali::verificUserPass()
{
    bool ingresa=false;
    map<string,string>::iterator it;
    it=m.find(user);
    if(it!=m.end()){
      if(desencriptador((*it).second)== desencriptador(pass)){ingresa=true;}
    }
    return ingresa;
}


void Uservali::readPass(char caracter)
{
    if (caracter != BACKSPACE) {
         pass.push_back(caracter + 5);
          cout << "*";

       } else {
           if (pass.length() > 0) {
               cout << "\b \b";                             //borro el asterisco que escribi
               pass = pass.substr(0, pass.length() - 1);    //le saco el ultimo caracter a la clave
           }
    }
}

string Uservali::desencriptador(string original)
{
        for(unsigned i=0; i<original.length(); i++)
        {
            original[i] -= 5;
        }
        return original;
}

void Uservali::savelist()
{
    ofstream out_archi("Users.bin",ios::out|ios::binary);
    string clave,valor;

    map<string,string>::iterator it=m.begin();

    short tamanio=0;
    while(it!=m.end()){
        clave=(*it).first;
        tamanio=clave.size();
        out_archi.write(reinterpret_cast<char*>(&tamanio),sizeof(short));
        out_archi.write(clave.c_str(),tamanio);

        valor=(*it).second;
        tamanio=valor.size();
        out_archi.write(reinterpret_cast<char*>(&tamanio),sizeof(short));
        out_archi.write(valor.c_str(),tamanio);
        ++it;
        clave="";
        valor="";
               }
    out_archi.close();
}

void Uservali::readlist()
{
    short tamanio=0;
    ifstream in_archi("Users.bin",ios::in|ios::binary);
    string clave="";
    string valor="";
    char *buff;

    while(in_archi.read((char*)&tamanio,sizeof(short))){
        buff = new char[tamanio];
        in_archi.read(buff,tamanio);
        clave = "";
        clave.append(buff,tamanio);
        delete buff;

        tamanio = 0;
        in_archi.read((char*)&tamanio,sizeof(short));
        buff = new char[tamanio];
        in_archi.read(buff,tamanio);
        valor="";
        valor.append(buff,tamanio);
        delete buff;
        m[clave]=valor;
        tamanio = 0;
        clave="";
        valor="";
    }
    in_archi.close();
}

bool Uservali::checkPass(string p)
{
    //min 8 caracteres,1 minuscula,1 mayuscula,1 numero
    bool resp=false;

    unsigned contMayus,contMinus,contNro,tamanio;
    contMayus=0;
    contMinus=0;
    contNro=0;
    tamanio=p.size();

    for(unsigned i=0;i<p.size();++i){
        if((p[i])>=65&&(p[i])<=90){++contMayus;}
        if((p[i])>=97&&(p[i])<=122){++contMinus;}
        if((p[i])>=48&&(p[i])<=57){++contNro;}
    }

    if(tamanio>=8&&contMayus>=1&&contMinus>=1&&contNro>=1){resp=true;}

    return resp;
}

string Uservali::listarusuarios()
{
    string clave;
    map<string,string>::iterator it=m.begin();
    string acumulador="\n";

    while(it!=m.end()){
        acumulador+=(*it).first;
        acumulador+="\n";
        ++it;
        clave="";
               }
    return acumulador;
}
