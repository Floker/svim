#ifndef USERVALI_H
#define USERVALI_H
#include"uservali.h"
#include "stdio.h"
#include <string>
#include<map>
#include<fstream>
#include <iterator>
#include<iterator>
using namespace std;

class Uservali
{
private:
    string pass;
    string user;
    bool newUser(string pUser,string pPass);
    bool modificarUser(string pUser, string pPass);
    bool verificUserPass();
    void readPass(char caracter);
    string desencriptador(string original);
    map <string,string> m;
    void savelist();
    void readlist();
    bool checkPass(string p);
    string listarusuarios();

public:
    Uservali();
    bool Run();
};

#endif // Uservali_H
